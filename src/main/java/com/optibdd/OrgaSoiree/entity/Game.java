package com.optibdd.OrgaSoiree.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "game", indexes = @Index(name = "idx_name_game", columnList = "game_name"))
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "game_id")
    private Long id;

    @Column(name = "game_name", nullable = false, length = 150)
    private String game_name;

    @Column(name = "platform", nullable = false, length = 50)
    private String platform;

    @Column(name = "nb_players", nullable = false)
    @Min(0)
    private Integer nb_players;
}
