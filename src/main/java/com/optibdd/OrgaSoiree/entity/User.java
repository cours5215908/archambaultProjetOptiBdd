package com.optibdd.OrgaSoiree.entity;

import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.dto.user.UserLoginDto;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;

@Entity
@Table(name = "users", indexes = @Index(name = "idx_username_user", columnList = "username"))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Cacheable
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "username", nullable = false, length = 150)
    private String username;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "birthdate", nullable = false)
    private LocalDate birthdate;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;

    @Column(name = "notation")
    @Min(0L)
    @Max(5L)
    private Float notation;

    @Column(name = "interests")
    private String interests;


    /*
    public User(UserDto userDto) {
        this.username = userDto.getUsername();
        this.email = userDto.getEmail();
        this.password = userDto.getPassword();
        this.firstname = userDto.getFirstname();
        this.lastname = userDto.getLastname();
        this.department = userDto.getDepartment();
        this.user_city = userDto.getUser_city();
        this.birthdate = userDto.getBirthdate();
        this.adresse = userDto.getAdresse();
        this.notation = null;
        interests = userDto.getInterests();
    }

    public User(UserLoginDto userLoginDto) {
        this.email = userLoginDto.getUsername();
        this.password = userLoginDto.getPassword();
    }
    */

}
