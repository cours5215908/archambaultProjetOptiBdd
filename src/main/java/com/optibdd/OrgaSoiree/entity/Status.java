package com.optibdd.OrgaSoiree.entity;

public enum Status {
    PENDING,
    CONFIRMED
}
