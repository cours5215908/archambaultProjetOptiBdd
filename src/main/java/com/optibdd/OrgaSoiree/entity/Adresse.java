package com.optibdd.OrgaSoiree.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "adresse")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Adresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adresse_id")
    private Long id;

    @Column(name = "ville")
    private String ville;

    @Column(name = "complementAdresse")
    private String complementAdresse;

    @Column(name = "codePostal")
    private int codePostal;

    @OneToOne(mappedBy = "adresse", fetch = FetchType.LAZY)
    private User user;

    @OneToOne(mappedBy = "adresse", fetch = FetchType.LAZY)
    private Party party;
}
