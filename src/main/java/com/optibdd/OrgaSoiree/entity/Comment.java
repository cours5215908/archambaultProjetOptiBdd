package com.optibdd.OrgaSoiree.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "comment", indexes = @Index(name = "idx_writeFrom_comment", columnList = "writeFrom"))
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Long id;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "comment_notation", nullable = false)
    @Min(0)
    @Max(5)
    private Integer notation;

    @Column(name = "comment_publication", nullable = false, updatable = false)
    private LocalDateTime publication;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "writeFrom")
    private User writeFrom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party")
    private Party party;

    @PrePersist
    protected void onCreate() {
        this.publication = LocalDateTime.now();
    }

}
