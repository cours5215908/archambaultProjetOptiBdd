package com.optibdd.OrgaSoiree.entity;

import com.optibdd.OrgaSoiree.dto.party.PartyFullDto;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "party", indexes = @Index(name = "idx_name_party", columnList = "party_name"))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Cacheable
public class Party {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "party_id")
    private Long id;

    @Column(name = "party_name", nullable = false, length = 100)
    private String name;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;

    @Column(name = "nb_person", nullable = false)
    @Min(0)
    private Integer nb_person = 0;

    @Column(name = "nb_person_max", nullable = false)
    @Min(0)
    private Integer nb_person_max;

    @Column(name = "price", nullable = false)
    @Min(0L)
    private Float price = 0f;

    @Column(name = "party_start", nullable = false)
    private LocalDateTime start;

    @Column(name = "need_apetizers", nullable = false)
    private Boolean need_apetizers;

    @Column(name = "details", nullable = false)
    private String details;

    @Column(name = "isFinish", nullable = false)
    private Boolean isFinish = false;

    @Column(name = "party_publication")
    private LocalDateTime publication;

    @Column(name = "thematique", nullable = false)
    private String thematique;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organisateur")
    private User organisateur;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "party_games", joinColumns = @JoinColumn(name = "party_id"), inverseJoinColumns = @JoinColumn(name = "game_id"))
    @BatchSize(size = 10)
    private List<Game> games;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "party_apetizers", joinColumns = @JoinColumn(name = "party_id"), inverseJoinColumns = @JoinColumn(name = "apetizer_id"))
    @BatchSize(size = 5)
    private List<Apetizer> apetizers;

    public Party(PartyFullDto partyFullDto) {
        this.name = partyFullDto.getName();
        this.nb_person = partyFullDto.getNb_person();
        this.nb_person_max = partyFullDto.getNb_person_max();
        this.price = partyFullDto.getPrice();
        this.start = partyFullDto.getStart();
        this.need_apetizers = partyFullDto.getNeed_apetizers();
        this.details = partyFullDto.getDetails();
        this.publication = LocalDateTime.now();
        this.thematique = partyFullDto.getThematique();
        this.organisateur = partyFullDto.getOrganisateur();
        this.games = partyFullDto.getGames();
        this.apetizers = partyFullDto.getApetizers();
        this.adresse = partyFullDto.getAdresse();
    }

    @PrePersist
    protected void onCreate() {
        this.publication = LocalDateTime.now();
    }
}
