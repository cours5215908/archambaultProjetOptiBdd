package com.optibdd.OrgaSoiree.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cglib.core.Local;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "participation", indexes = {@Index(name = "idx_party_participation", columnList = "participation_party_id"),
                                            @Index(name = "idx_user_participation", columnList = "participant_id")})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Cacheable
public class Participation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participation_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "participation_status", nullable = false, length = 50)
    private Status status = Status.PENDING;

    @Column(name = "participation_created_at", nullable = false, updatable = false)
    private LocalDateTime created_at;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participant_id")
    private User participant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participation_party_id")
    private Party party;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "participation_games", joinColumns = @JoinColumn(name = "participation_id"), inverseJoinColumns = @JoinColumn(name = "game_id"))
    @BatchSize(size = 10)
    private List<Game> games;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "participation_apetizers", joinColumns = @JoinColumn(name = "participation_id"), inverseJoinColumns = @JoinColumn(name = "apetizer_id"))
    @BatchSize(size = 5)
    private List<Apetizer> apetizers;

    @PrePersist
    protected void onCreate() {
        this.created_at = LocalDateTime.now();
    }
}
