package com.optibdd.OrgaSoiree.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "aperitif", indexes = @Index(name = "idx_name_aperitif", columnList = "apetizer_name"))
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Apetizer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "apetizer_id")
    private Long id;

    @Column(name = "apetizer_name", nullable = false, length = 200)
    private String apetizer_name;

    @Column(name = "quantity", nullable = false)
    @Min(0L)
    private Float quantity;
}
