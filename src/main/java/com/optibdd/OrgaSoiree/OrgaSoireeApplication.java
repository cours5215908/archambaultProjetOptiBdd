package com.optibdd.OrgaSoiree;

import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Theme;
import com.optibdd.OrgaSoiree.entity.User;
import com.optibdd.OrgaSoiree.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;

@SpringBootApplication
@EnableCaching
@EnableJpaRepositories
public class OrgaSoireeApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrgaSoireeApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(UserService userService) {
		return args -> {
			if (!userService.existsByEmail("admin1@yopmail.com")) {
				UserDto user = new UserDto(
						1L,
						"admin1",
						"admin1@yopmail.com",
						"Passxord123",
						"Jon",
						"Doe",
						LocalDate.of(1998,12,2),
						Theme.CLASSIQUE.name(),
						new Adresse()
						);
				userService.register(user);
			}
		};
	}
}
