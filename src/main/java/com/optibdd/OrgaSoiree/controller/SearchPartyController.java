package com.optibdd.OrgaSoiree.controller;

import com.optibdd.OrgaSoiree.dto.party.PartyDto;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import com.optibdd.OrgaSoiree.service.PartyService;
import com.optibdd.OrgaSoiree.service.UserService;
import com.optibdd.OrgaSoiree.service.security.JwtService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/parties")
@AllArgsConstructor
public class SearchPartyController {

    private final PartyService partyService;

    @GetMapping("/searchByName/{name}")
    public ResponseEntity<PartyDto> findAllByName(@RequestParam(defaultValue = "0") int page,
                                                         @RequestParam(defaultValue = "15") int size,
                                                         @PathVariable(value="name") String name) {
        return ResponseEntity.ok(partyService.findPartyByName(name));
    }

    @GetMapping("/searchByCity/{city}")
    public ResponseEntity<Page<PartyDto>> findAllByCity(@RequestParam(defaultValue = "0") int page,
                                                        @RequestParam(defaultValue = "15") int size,
                                                        @PathVariable(value="city") String city) {
        return ResponseEntity.ok(partyService.findPartiesByParty_city(city, page, size));
    }

    @GetMapping("/searchByTheme/{theme}")
    public ResponseEntity<Page<PartyDto>> findAllByTheme(@RequestParam(defaultValue = "0") int page,
                                                        @RequestParam(defaultValue = "15") int size,
                                                        @PathVariable(value="theme") String theme) {
        return ResponseEntity.ok(partyService.findPartiesByThematique(theme, page, size));
    }

    @GetMapping("/searchByPrice/{price}")
    public ResponseEntity<Page<PartyDto>> findAllByTheme(@RequestParam(defaultValue = "0") int page,
                                                         @RequestParam(defaultValue = "15") int size,
                                                         @PathVariable(value="price") Float price) {
        return ResponseEntity.ok(partyService.findPartiesByPrice(price, page, size));
    }


}
