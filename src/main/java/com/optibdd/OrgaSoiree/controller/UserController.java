package com.optibdd.OrgaSoiree.controller;

import com.optibdd.OrgaSoiree.dto.user.JwtResponse;
import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.dto.user.UserLoginDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Theme;
import com.optibdd.OrgaSoiree.entity.User;
import com.optibdd.OrgaSoiree.service.UserService;
import com.optibdd.OrgaSoiree.service.security.CustomUserDetailsService;
import com.optibdd.OrgaSoiree.service.security.JwtService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;


    private boolean verifyUser(String token, Long userId) {
        String userEmail = jwtService.extractUsername(token);
        UserDto actualUser = userService.findByEmail(userEmail);

        return Objects.equals(actualUser.getId(), userId);
    }


    @Autowired
    public UserController(UserService userService, AuthenticationManager authenticationManager, JwtService jwtService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
    }

    @PostMapping("/sign-up")
    public Object signUp(@RequestBody UserDto user) {
        try {
            if (userService.existsByEmail(user.getEmail())) {
                return ResponseEntity.badRequest().body("Error while trying to register user.");
            } else {
                return ResponseEntity.ok(userService.register(user));
            }

        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping("/update/{userId}")
    public ResponseEntity<UserDto> update(@PathVariable Long userId, @RequestBody UserDto user, @RequestHeader(name="Authorization") String token) {

        if(!verifyUser(token, userId)) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(userService.update(userId, user));
    }

    @DeleteMapping("/delete/{userId}")
    public ResponseEntity<String> delete(@PathVariable Long userId, @RequestHeader(name="Authorization") String token) {

        if(!verifyUser(token, userId)) {
            return ResponseEntity.badRequest().build();
        }

        userService.deleteUserById(userId);
        return ResponseEntity.ok("Success");
    }

    @GetMapping("/profile")
    public ResponseEntity<UserDto> myProfile( @RequestHeader(name="Authorization") String token) {
        String userEmail = jwtService.extractUsername(token);
        return ResponseEntity.ok(userService.findByEmail(userEmail));
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody UserLoginDto requestBody) {
        if (userService.existsByEmail(requestBody.getEmail())) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            requestBody.getEmail(),
                            requestBody.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String token = jwtService.createToken(
                    List.of(new SimpleGrantedAuthority("USER")),
                    authentication.getName()
            );
            return ResponseEntity.ok(JwtResponse.from(token));
        }
        return ResponseEntity.notFound().build();
    }
}
