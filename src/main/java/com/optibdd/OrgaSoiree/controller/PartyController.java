package com.optibdd.OrgaSoiree.controller;

import com.optibdd.OrgaSoiree.dto.AdresseDto;
import com.optibdd.OrgaSoiree.dto.party.PartyDto;
import com.optibdd.OrgaSoiree.dto.party.PartyFullDto;
import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.mapper.AdresseMapper;
import com.optibdd.OrgaSoiree.mapper.party.PartyFullMapper;
import com.optibdd.OrgaSoiree.mapper.party.PartyMapper;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import com.optibdd.OrgaSoiree.service.AdresseService;
import com.optibdd.OrgaSoiree.service.PartyService;
import com.optibdd.OrgaSoiree.service.UserService;
import com.optibdd.OrgaSoiree.service.security.JwtService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/parties")
@AllArgsConstructor
public class PartyController {

    private final PartyService partyService;
    private final JwtService jwtService;
    private final UserService userService;
    private final UserMapper userMapper;

    @PostMapping
    public ResponseEntity<PartyFullDto> createParty(@RequestBody PartyFullDto partyFullDto, @RequestHeader(name="Authorization") String token) {
        String userEmail = jwtService.extractUsername(token);
        UserDto actualUser = userService.findByEmail(userEmail);

        partyFullDto.setOrganisateur(userMapper.toEntity(actualUser));

        return ResponseEntity.ok(partyService.saveParty(partyFullDto));
    }

    @GetMapping
    public ResponseEntity<Page<PartyDto>> findAll(@RequestParam(defaultValue = "0") int page,
                                                  @RequestParam(defaultValue = "15") int size) {
        return ResponseEntity.ok(partyService.findAll(page, size));
    }

    @GetMapping("/{partyId}")
    public ResponseEntity<PartyFullDto> getDetailsParty(@PathVariable Long partyId) {
        return ResponseEntity.ok(partyService.findPartyById(partyId));
    }

    @PutMapping("/{partyId}")
    public ResponseEntity<PartyFullDto> update(@PathVariable Long partyId, @RequestBody PartyFullDto newParty, @RequestHeader(name="Authorization") String token) {
        String userEmail = jwtService.extractUsername(token);
        UserDto actualUser = userService.findByEmail(userEmail);

        PartyFullDto oldParty = partyService.findPartyById(partyId);

        if(!Objects.equals(actualUser.getId(), oldParty.getOrganisateur().getId())) {
            return ResponseEntity.badRequest().build();
        }

        newParty.setOrganisateur(userMapper.toEntity(actualUser));

        return ResponseEntity.ok(partyService.updatePartyById(partyId, newParty));
    }

    @DeleteMapping("/{partyId}")
    public ResponseEntity<String> delete(@PathVariable Long partyId, @RequestHeader(name="Authorization") String token) {

        String userEmail = jwtService.extractUsername(token);
        UserDto actualUser = userService.findByEmail(userEmail);

        PartyFullDto partyFullDto1 = partyService.findPartyById(partyId);

        if(!Objects.equals(actualUser.getId(), partyFullDto1.getOrganisateur().getId())) {
            return ResponseEntity.badRequest().build();
        }

        partyService.deleteParty(partyFullDto1);
        return ResponseEntity.ok("Success");
    }




}
