package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    Boolean existsUserByEmail(String email);

    boolean existsByUsername(String username);
}
