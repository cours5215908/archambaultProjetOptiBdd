package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
