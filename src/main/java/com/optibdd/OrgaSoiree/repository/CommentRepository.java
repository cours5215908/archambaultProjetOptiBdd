package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.Comment;
import com.optibdd.OrgaSoiree.entity.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("SELECT c FROM Comment c WHERE c.party = :commentsByPartyId")
    Page<Comment> findCommentsByPartyId(@Param("commentsByPartyId") Long commentsByPartyId, Pageable pageable);
}
