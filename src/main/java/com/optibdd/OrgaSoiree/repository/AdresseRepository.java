package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Party;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long> {
    Adresse findAdresseByParty(Party party);
}
