package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.Apetizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApetizerRepository extends JpaRepository<Apetizer, Long> {
}
