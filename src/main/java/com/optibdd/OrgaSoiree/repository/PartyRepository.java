package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.Participation;
import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.entity.Status;
import com.optibdd.OrgaSoiree.entity.Theme;
import jakarta.servlet.http.Part;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface PartyRepository extends JpaRepository<Party, Long> {

    @Query("SELECT p FROM Party p WHERE p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findAllParties(Pageable pageable);

    @Query("SELECT p FROM Party p WHERE p.name = :name AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Party findPartyByName(String name);

    @Query("SELECT p FROM Party p LEFT JOIN p.adresse pa WHERE pa.ville = :city AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findPartiesByParty_city(String city, Pageable pageable);

    @Query("SELECT p FROM Party p WHERE p.thematique = :theme AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findPartiesByThematique(String theme, Pageable pageable);

    @Query("SELECT p FROM Party p WHERE p.nb_person = :nb AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findPartiesByNb_person(Integer nb, Pageable pageable);

    @Query("SELECT p FROM Party p WHERE p.price = :price AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findPartiesByPrice(Float price, Pageable pageable);

    @Query("SELECT p FROM Party p WHERE p.start <= :dateTime AND p.nb_person < p.nb_person_max ORDER BY p.start")
    Page<Party> findPartiesByStartAfter(LocalDateTime dateTime, Pageable pageable);
}
