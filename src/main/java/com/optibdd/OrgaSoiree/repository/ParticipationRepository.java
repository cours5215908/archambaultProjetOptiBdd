package com.optibdd.OrgaSoiree.repository;

import com.optibdd.OrgaSoiree.entity.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Long> {

    @Query("SELECT p FROM Participation p WHERE p.status = :status AND p.party = :partyId")
    Page<Participation> findParticipationsByStatusAndPartyId(Status status, @Param("partyId") Long partyId, Pageable pageable);

    @Query("SELECT p FROM Participation p WHERE p.party = :partyId")
    Page<Participation> findParticipationsByPartyId(@Param("partyId") Long partyId, Pageable pageable);

    @Query("SELECT p.apetizers FROM Participation p WHERE p.party = :partyId")
    Page<Apetizer> findApetizersByPartyId(@Param("partyId") Long partyId, Pageable pageable);

    @Query("SELECT p.games FROM Participation p WHERE p.party = :partyId")
    Page<Game> findGamesByPartyId(@Param("partyId") Long partyId, Pageable pageable);
}
