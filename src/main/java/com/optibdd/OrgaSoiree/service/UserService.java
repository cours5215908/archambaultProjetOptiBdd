package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.AdresseDto;
import com.optibdd.OrgaSoiree.dto.GameDto;
import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Game;
import com.optibdd.OrgaSoiree.entity.User;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import com.optibdd.OrgaSoiree.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final BCryptPasswordEncoder encoder;

    public UserDto register(UserDto request) {
        request.setPassword(encoder.encode(request.getPassword()));
        User user = userMapper.toEntity(request);
        return userMapper.toDto(userRepository.save(user));
    }

    public UserDto findUserById(Long id) {
        return userMapper.toDto(userRepository.findById(id).orElse(null));
    }

    public Page<UserDto> findAllUsers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> users = userRepository.findAll(pageable);
        return users.map(userMapper::toDto);
    }

    @CacheEvict(value = "commentsForUserId", key = "#id")
    public UserDto update(Long id, UserDto userDto) {
        return userRepository.findById(id)
                .map(existingUser -> {
                    User user = userMapper.toEntity(userDto);
                    user.setId(id);
                    return userMapper.toDto(userRepository.save(user));
                })
                .orElse(null);
    }

    @CacheEvict(value = "commentsForUserId", key = "#id")
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsUserByEmail(email);
    }

    public UserDto findByEmail(String email) {
        return userMapper.toDto(userRepository.findByEmail(email));
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }
}
