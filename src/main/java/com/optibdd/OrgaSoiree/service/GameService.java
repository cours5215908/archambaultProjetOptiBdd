package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.GameDto;
import com.optibdd.OrgaSoiree.entity.Game;
import com.optibdd.OrgaSoiree.mapper.GameMapper;
import com.optibdd.OrgaSoiree.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GameService {

    private final GameRepository gameRepository;
    private final GameMapper gameMapper;

    public GameDto saveGame(Game game) {
        return gameMapper.toDto(gameRepository.save(game));
    }

    public GameDto findGameById(Long id) {
        return gameMapper.toDto(gameRepository.findById(id).orElse(null));
    }

    public Page<GameDto> findAllGames(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Game> games = gameRepository.findAll(pageable);
        return games.map(gameMapper::toDto);
    }

    public void deleteGameById(Long id) {
        gameRepository.deleteById(id);
    }
}
