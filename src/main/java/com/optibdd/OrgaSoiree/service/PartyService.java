package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.ApetizerDto;
import com.optibdd.OrgaSoiree.dto.party.PartyDto;
import com.optibdd.OrgaSoiree.dto.party.PartyFullDto;
import com.optibdd.OrgaSoiree.entity.Apetizer;
import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.entity.Theme;
import com.optibdd.OrgaSoiree.entity.User;
import com.optibdd.OrgaSoiree.mapper.GameMapper;
import com.optibdd.OrgaSoiree.mapper.party.PartyFullMapper;
import com.optibdd.OrgaSoiree.mapper.party.PartyMapper;
import com.optibdd.OrgaSoiree.repository.PartyRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PartyService {

    private final PartyRepository partyRepository;
    private final PartyMapper partyMapper;
    private final PartyFullMapper partyFullMapper;

    @Caching(/*
            cacheable = {
            @Cacheable(value = "partiesByCity", key = "#partyFullDto.adresse.ville"),
            @Cacheable(value = "partiesByTheme", key = "#partyFullDto.thematique"),
            @Cacheable(value = "partiesByPrice", key = "#partyFullDto.price"),
            @Cacheable(value = "partiesByStartAfter", key = "#partyFullDto.start"),
    },*/
            evict = @CacheEvict(value = "partiesAll", allEntries = true))
    public PartyFullDto saveParty(PartyFullDto partyFullDto) {
        Party party = new Party(partyFullDto);
        return partyFullMapper.toDto(partyRepository.save(party));
    }

    public PartyFullDto findPartyById(Long id) {
        return partyFullMapper.toDto(partyRepository.findById(id).orElse(null));
    }

    @Caching(put = {
            @CachePut(value = "partiesByCity", key = "#partyFullDto.adresse.ville"),
            @CachePut(value = "partiesByTheme", key = "#partyFullDto.thematique"),
            @CachePut(value = "partiesByPrice", key = "#partyFullDto.price"),
            @CachePut(value = "partiesByStartAfter", key = "#partyFullDto.start"),
    },
            evict = @CacheEvict(value = "partiesAll", allEntries = true))
    public PartyFullDto updatePartyById(Long id, PartyFullDto partyFullDto) {
        return partyRepository.findById(id)
                .map(existingParty -> {
                    Party party = partyFullMapper.toEntity(partyFullDto);
                    party.setId(id);
                    return partyFullMapper.toDto(partyRepository.save(party));
                })
                .orElse(null);
    }

    @Cacheable(value = "partiesAll")
    public Page<PartyDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findAllParties(pageable);
        return parties.map(partyMapper::toDto);
    }

    @Caching(evict = {
            @CacheEvict(value = "partiesByCity", key = "#partyFullDto.adresse.ville"),
            @CacheEvict(value = "partiesByTheme", key = "#partyFullDto.thematique"),
            @CacheEvict(value = "partiesByPrice", key = "#partyFullDto.price"),
            @CacheEvict(value = "partiesByStartAfter", key = "#partyFullDto.start"),
            @CacheEvict(value = "partiesAll", allEntries = true)
    })
    public void deleteParty(PartyFullDto partyFullDto) {
        partyRepository.deleteById(partyFullDto.getId());
    }

    public PartyDto findPartyByName(String name) {
        Party party = partyRepository.findPartyByName(name);
        return partyMapper.toDto(party);
    }

    @Cacheable(value = "partiesByCity", key = "#city")
    public Page<PartyDto> findPartiesByParty_city(String city, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findPartiesByParty_city(city, pageable);
        return parties.map(partyMapper::toDto);
    }

    @Cacheable(value = "partiesByTheme", key = "#theme")
    public Page<PartyDto> findPartiesByThematique(String theme, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findPartiesByThematique(theme, pageable);
        return parties.map(partyMapper::toDto);
    }

    public Page<PartyDto> findPartiesByNb_person(int nb_person, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findPartiesByNb_person(nb_person, pageable);
        return parties.map(partyMapper::toDto);
    }

    @Cacheable(value = "partiesByPrice", key = "#price")
    public Page<PartyDto> findPartiesByPrice(float price, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findPartiesByPrice(price, pageable);
        return parties.map(partyMapper::toDto);
    }

    @Cacheable(value = "partiesByStartAfter", key = "#dateTime")
    public Page<PartyDto> findPartiesByStartAfter(LocalDateTime dateTime, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> parties = partyRepository.findPartiesByStartAfter(dateTime, pageable);
        return parties.map(partyMapper::toDto);
    }
}
