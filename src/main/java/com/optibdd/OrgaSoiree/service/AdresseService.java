package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.AdresseDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.mapper.AdresseMapper;
import com.optibdd.OrgaSoiree.repository.AdresseRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AdresseService {

    private final AdresseRepository adresseRepository;
    private final AdresseMapper adresseMapper;

    public Page<AdresseDto> findAllAdresse(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Adresse> adresses = adresseRepository.findAll(pageable);
        return adresses.map(adresseMapper::toDto);
    }

    public AdresseDto findAdresseById(Long id) {
        return adresseMapper.toDto(adresseRepository.findById(id).orElse(null));
    }

    @Cacheable(value = "adresseByParty", key = "#partyId")
    public AdresseDto findAdresseByParty(Party party) {
        return adresseMapper.toDto(adresseRepository.findAdresseByParty(party));
    }

    public AdresseDto saveAdresse(Adresse adresse) {
        return adresseMapper.toDto(adresseRepository.save(adresse));
    }

    @CacheEvict(value = "adresseByParty", allEntries = true)
    public AdresseDto updateAdresseById(Long id, AdresseDto adresseDto) {
        return adresseRepository.findById(id)
                .map(existingAdresse -> {
                    Adresse adresse = adresseMapper.toEntity(adresseDto);
                    adresse.setId(id);
                    return adresseMapper.toDto(adresseRepository.save(adresse));
                })
                .orElse(null);
    }

    @CacheEvict(value = "adresseByParty", allEntries = true)
    public void deleteAdresseById(Long id) {
        adresseRepository.deleteById(id);
    }
}
