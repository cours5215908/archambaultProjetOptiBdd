package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.ApetizerDto;
import com.optibdd.OrgaSoiree.entity.Apetizer;
import com.optibdd.OrgaSoiree.mapper.ApetizerMapper;
import com.optibdd.OrgaSoiree.repository.ApetizerRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ApetizerService {

    private final ApetizerRepository apetizerRepository;
    private final ApetizerMapper apetizerMapper;


    public ApetizerDto saveApetizer(Apetizer apetizer) {
        return apetizerMapper.toDto(apetizerRepository.save(apetizer));
    }

    public ApetizerDto findApetizerById(Long id) {
        return apetizerMapper.toDto(apetizerRepository.findById(id).orElse(null));
    }

    public Page<ApetizerDto> findAllApetizers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Apetizer> apetizerPage = apetizerRepository.findAll(pageable);
        return apetizerPage.map(apetizerMapper::toDto);
    }

    public ApetizerDto updateApetizerById(Long id, ApetizerDto apetizerDto) {
        return apetizerRepository.findById(id)
                .map(existingApetizer -> {
                    Apetizer apetizer = apetizerMapper.toEntity(apetizerDto);
                    apetizer.setId(id);
                    return apetizerMapper.toDto(apetizerRepository.save(apetizer));
                })
                .orElse(null);
    }

    public void deleteApetizerById(Long id) {
        apetizerRepository.deleteById(id);
    }
}
