package com.optibdd.OrgaSoiree.service.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

@Service
public class JwtService {
    public static final String ROLE = "ROLE";

    private static final String secret = "68c5985bfcefsdfdsfmpdza382705f8b747cadbaeda99060f1bc56c25112a88aff68594e5aa";

    public String createToken(List<SimpleGrantedAuthority> claims, String username) {
        Instant now = Instant.now();
        Instant expiration = Instant.now().plus(Period.ofWeeks(1));

        return Jwts.builder()
                .issuer("OrgaSoiree")
                .claim(ROLE, claims)
                .subject(username)
                .issuedAt(Date.from(now))
                .expiration(Date.from(expiration
                ))
                .signWith(getSignKey())
                .compact();
    }


    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    public String extractUsername(String token) {
        if (token.startsWith("Bearer ")) {
            return extractClaim(token.substring(7), Claims::getSubject);
        }

        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private static SecretKey getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith(getSignKey())
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }
}
