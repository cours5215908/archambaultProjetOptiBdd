package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.AdresseDto;
import com.optibdd.OrgaSoiree.dto.CommentDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Comment;
import com.optibdd.OrgaSoiree.mapper.CommentMapper;
import com.optibdd.OrgaSoiree.repository.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    @Cacheable(value = "commentsForPartyId", key = "#comment.party")
    public CommentDto saveComment(Comment comment) {
        return commentMapper.toDto(commentRepository.save(comment));
    }

    public CommentDto findCommentById(Long id) {
        return commentMapper.toDto(commentRepository.findById(id).orElse(null));
    }

    @Cacheable(value = "commentsForPartyId", key = "#commentsByPartyId")
    public Page<CommentDto> findAllCommentsForPartyId(Long commentsByPartyId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> comments = commentRepository.findCommentsByPartyId(commentsByPartyId, pageable);
        return comments.map(commentMapper::toDto);
    }

    public Page<CommentDto> findAllComments(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> comments = commentRepository.findAll(pageable);
        return comments.map(commentMapper::toDto);
    }

    @CacheEvict(value = "commentsForPartyId", allEntries = true)
    public void deleteCommentById(Long id) {
        commentRepository.deleteById(id);
    }
}
