package com.optibdd.OrgaSoiree.service;

import com.optibdd.OrgaSoiree.dto.*;
import com.optibdd.OrgaSoiree.entity.*;
import com.optibdd.OrgaSoiree.mapper.ApetizerMapper;
import com.optibdd.OrgaSoiree.mapper.GameMapper;
import com.optibdd.OrgaSoiree.mapper.ParticipationMapper;
import com.optibdd.OrgaSoiree.repository.ParticipationRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ParticipationService {

    private final ParticipationRepository participationRepository;
    private final ParticipationMapper participationMapper;
    private final ApetizerMapper apetizerMapper;
    private final GameMapper gameMapper;

    @Caching(cacheable = {
            @Cacheable(value = "participationsByStatusAndPartyId", key = "#participation.party.id"),
            @Cacheable(value = "participationsByPartyId", key = "#participation.party.id"),
            @Cacheable(value = "apetizersByPartyId", key = "#participation.party.id"),
            @Cacheable(value = "gamesByPartyId", key = "#participation.party.id"),
            @Cacheable(value = "participations", key = "#participation.id")
    })
    public ParticipationDto saveParticipation(Participation participation) {
        return participationMapper.toDto(participationRepository.save(participation));
    }

    @Cacheable(value = "participations", key = "#id")
    public ParticipationDto findParticipationById(Long id) {
        return participationMapper.toDto(participationRepository.findById(id).orElse(null));
    }

    public Page<ParticipationDto> findAllParticipation(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Participation> participations = participationRepository.findAll(pageable);
        return participations.map(participationMapper::toDto);
    }

    @Caching(put = {
            @CachePut(value = "participations", key = "#id"),
            @CachePut(value = "participationsByStatusAndPartyId", key = "#participationDto.party.id"),
            @CachePut(value = "participationsByPartyId", key = "#participationDto.party.id"),
            @CachePut(value = "apetizersByPartyId", key = "#participationDto.party.id"),
            @CachePut(value = "gamesByPartyId", key = "#participationDto.party.id"),
    })
    public ParticipationDto updateParticipationById(Long id, ParticipationDto participationDto) {
        return participationRepository.findById(id)
                .map(existingParticipation -> {
                    Participation participation = participationMapper.toEntity(participationDto);
                    participation.setId(id);
                    return participationMapper.toDto(participationRepository.save(participation));
                })
                .orElse(null);
    }

    @Caching( evict = {
            @CacheEvict(value = "participations", key = "#participationDto.id"),
            @CacheEvict(value = "participationsByPartyId", key = "#participationDto.party.id"),
            @CacheEvict(value = "apetizersByPartyId", key = "#participationDto.party.id"),
            @CacheEvict(value = "gamesByPartyId", key = "#participationDto.party.id"),
            @CacheEvict(value = "participationsByStatusAndPartyId", key = "#participationDto.party.id")
    })
    public void deleteParticipation(ParticipationDto participationDto) {
        participationRepository.delete(participationMapper.toEntity(participationDto));
    }

    @Cacheable(value = "participationsByStatusAndPartyId", key = "#partyId")
    public Page<ParticipationDto> findParticipationsByStatusAndPartyId(Status status, Long partyId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Participation> participations = participationRepository.findParticipationsByStatusAndPartyId(status, partyId, pageable);
        return participations.map(participationMapper::toDto);
    }

    @Cacheable(value = "participationsByPartyId", key = "#partyId")
    public Page<ParticipationDto> findParticipationsByPartyId(Long partyId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Participation> participations = participationRepository.findParticipationsByPartyId(partyId, pageable);
        return participations.map(participationMapper::toDto);
    }

    @Cacheable(value = "apetizersByPartyId", key = "#partyId")
    public Page<ApetizerDto> findApetizersByPartyId(Long partyId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Apetizer> apetizers = participationRepository.findApetizersByPartyId(partyId, pageable);
        return apetizers.map(apetizerMapper::toDto);
    }

    @Cacheable(value = "gamesByPartyId", key = "#partyId")
    public Page<GameDto> findGamesByPartyId(Long partyId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Game> games = participationRepository.findGamesByPartyId(partyId, pageable);
        return games.map(gameMapper::toDto);
    }
}
