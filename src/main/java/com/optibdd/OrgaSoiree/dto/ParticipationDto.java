package com.optibdd.OrgaSoiree.dto;

import com.optibdd.OrgaSoiree.entity.Apetizer;
import com.optibdd.OrgaSoiree.entity.Game;
import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.entity.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParticipationDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private LocalDateTime created_at;
    private User participant;
    private Party party;
    private List<Game> games;
    private List<Apetizer> apetizers;
}
