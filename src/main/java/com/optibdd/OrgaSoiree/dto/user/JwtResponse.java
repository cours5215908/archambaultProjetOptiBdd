package com.optibdd.OrgaSoiree.dto.user;

public record JwtResponse(
        String accessToken
) {
    public static JwtResponse from(String token) {
        return new JwtResponse(token);
    }

    public static JwtResponse empty() {
        return new JwtResponse(null);
    }
}
