package com.optibdd.OrgaSoiree.dto.user;

import com.optibdd.OrgaSoiree.entity.Adresse;
import com.optibdd.OrgaSoiree.entity.Theme;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String username;
    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private LocalDate birthdate;
    private String interests;
    private Adresse adresse;
}
