package com.optibdd.OrgaSoiree.dto.party;

import com.optibdd.OrgaSoiree.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartyDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String name;
    private Integer nb_person;
    private Integer nb_person_max;
    private Float price;
    private Adresse adresse;
    private LocalDateTime start;
    private String thematique;
}
