package com.optibdd.OrgaSoiree.dto.party;

import com.optibdd.OrgaSoiree.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartyFullDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String name;
    private Integer nb_person;
    private Integer nb_person_max;
    private Float price;
    private LocalDateTime start;
    private Boolean need_apetizers;
    private String details;
    private LocalDateTime publication;

    private String thematique;
    private User organisateur;

    private List<Game> games;
    private List<Apetizer> apetizers;

    private Adresse adresse;
}
