package com.optibdd.OrgaSoiree.dto;

import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.entity.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String title;
    private String text;
    private Integer notation;
    private LocalDateTime publication;
    private User writeFrom;
    private Party party;
}
