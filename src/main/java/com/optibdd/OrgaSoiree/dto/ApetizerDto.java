package com.optibdd.OrgaSoiree.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApetizerDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String apetizer_name;
    private Float quantity;
}
