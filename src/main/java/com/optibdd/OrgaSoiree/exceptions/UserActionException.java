package com.optibdd.OrgaSoiree.exceptions;

public class UserActionException extends RuntimeException {

    private static final String MESSAGE_FORMAT = "Utilisateur %s n'a pas été enregistré dans la base.";

    public UserActionException(Object value) {
        super(String.format(MESSAGE_FORMAT, value));
    }

    public UserActionException(Object value, Throwable cause) {
        super(String.format(MESSAGE_FORMAT, value), cause);
    }
}
