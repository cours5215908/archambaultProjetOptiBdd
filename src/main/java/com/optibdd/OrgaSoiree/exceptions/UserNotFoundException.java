package com.optibdd.OrgaSoiree.exceptions;

public class UserNotFoundException extends RuntimeException {

    private static final String MESSAGE_FORMAT = "Utilisateur avec l'attribut %s(%s) est introuvable.";

    public UserNotFoundException(String attribut, Object value) {
        super(String.format(MESSAGE_FORMAT, attribut, value));
    }

    public UserNotFoundException(String attribut, Object value, Throwable cause) {
        super(String.format(MESSAGE_FORMAT, attribut, value), cause);
    }
}
