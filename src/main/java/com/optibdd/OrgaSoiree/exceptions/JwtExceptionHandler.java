package com.optibdd.OrgaSoiree.exceptions;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class JwtExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<Object> handleExpiredJwtException(ExpiredJwtException e, WebRequest request) {
        return handleExceptionInternal(e, "{\"message\": \"Token expired\"}", new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<Object> handleCorruptJwtException(SignatureException e, WebRequest request) {
        return handleExceptionInternal(e, "{\"message\": \"🤡\"}", new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }
}
