package com.optibdd.OrgaSoiree.mapper.user;

import com.optibdd.OrgaSoiree.dto.user.UserDto;
import com.optibdd.OrgaSoiree.entity.User;
import com.optibdd.OrgaSoiree.mapper.AdresseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {AdresseMapper.class})
public interface UserMapper {
    @Mapping(target = "adresse.user", ignore = true)
    UserDto toDto(User user);
    User toEntity(UserDto userDto);
    List<UserDto> toDtos(List<User> users);
    List<User> toEntities(List<UserDto> userDtos);
}
