package com.optibdd.OrgaSoiree.mapper.user;

import com.optibdd.OrgaSoiree.dto.user.UserLoginDto;
import com.optibdd.OrgaSoiree.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserLoginMapper {
    UserLoginDto toDto(User user);
    User toEntity(UserLoginDto userLoginDto);
    List<UserLoginDto> toDtos(List<User> users);
    List<User> toEntities(List<UserLoginDto> userLoginDtos);
}
