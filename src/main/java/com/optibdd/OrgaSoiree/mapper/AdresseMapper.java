package com.optibdd.OrgaSoiree.mapper;

import com.optibdd.OrgaSoiree.dto.AdresseDto;
import com.optibdd.OrgaSoiree.entity.Adresse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AdresseMapper {
    AdresseDto toDto(Adresse adresse);
    Adresse toEntity(AdresseDto adresseDto);
    List<AdresseDto> toDtos(List<Adresse> adresses);
    List<Adresse> toEntities(List<AdresseDto> adressesDto);
}
