package com.optibdd.OrgaSoiree.mapper;

import com.optibdd.OrgaSoiree.dto.GameDto;
import com.optibdd.OrgaSoiree.entity.Game;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameMapper {
    GameDto toDto(Game game);
    Game toEntity(GameDto gameDto);
    List<GameDto> toDtos(List<Game> games);
    List<Game> toEntities(List<GameDto> gameDtos);
}
