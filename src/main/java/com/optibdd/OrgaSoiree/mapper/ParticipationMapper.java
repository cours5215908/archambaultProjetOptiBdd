package com.optibdd.OrgaSoiree.mapper;

import com.optibdd.OrgaSoiree.dto.ParticipationDto;
import com.optibdd.OrgaSoiree.entity.Participation;
import com.optibdd.OrgaSoiree.mapper.party.PartyMapper;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {UserMapper.class, GameMapper.class, ApetizerMapper.class, PartyMapper.class})
public interface ParticipationMapper {
    ParticipationDto toDto(Participation participation);
    Participation toEntity(ParticipationDto participationDto);
    List<ParticipationDto> toDtos(List<Participation> participations);
    List<Participation> toEntities(List<ParticipationDto> participationDtos);
}
