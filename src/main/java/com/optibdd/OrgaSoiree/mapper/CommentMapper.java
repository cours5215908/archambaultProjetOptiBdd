package com.optibdd.OrgaSoiree.mapper;

import com.optibdd.OrgaSoiree.dto.CommentDto;
import com.optibdd.OrgaSoiree.entity.Comment;
import com.optibdd.OrgaSoiree.mapper.party.PartyMapper;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {PartyMapper.class, UserMapper.class})
public interface CommentMapper {
    CommentDto toDto(Comment comment);
    Comment toEntity(CommentDto commentDto);
    List<CommentDto> toDtos(List<Comment> comments);
    List<Comment> toEntities(List<CommentDto> commentDtos);
}
