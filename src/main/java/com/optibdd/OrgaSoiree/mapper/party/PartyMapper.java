package com.optibdd.OrgaSoiree.mapper.party;

import com.optibdd.OrgaSoiree.dto.party.PartyDto;
import com.optibdd.OrgaSoiree.entity.Party;
import com.optibdd.OrgaSoiree.mapper.*;
import com.optibdd.OrgaSoiree.mapper.user.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {AdresseMapper.class})
public interface PartyMapper {
    @Mapping(target = "adresse.party", ignore = true)
    @Mapping(target = "adresse.user", ignore = true)
    @Mapping(target = "adresse.complementAdresse", ignore = true)
    PartyDto toDto(Party party);
    Party toEntity(PartyDto partyDto);
    List<PartyDto> toDtos(List<Party> parties);
    List<Party> toEntities(List<PartyDto> partyDtos);
}
