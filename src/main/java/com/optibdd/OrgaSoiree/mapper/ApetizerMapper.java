package com.optibdd.OrgaSoiree.mapper;

import com.optibdd.OrgaSoiree.dto.ApetizerDto;
import com.optibdd.OrgaSoiree.entity.Apetizer;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ApetizerMapper {
    ApetizerDto toDto(Apetizer apetizer);
    Apetizer toEntity(ApetizerDto apetizerDto);
    List<ApetizerDto> toDtos(List<Apetizer> apetizers);
    List<Apetizer> toEntities(List<ApetizerDto> apetizerDtos);
}
